module scripts

import json
import os

pub struct Machine {
    pub:
    accessible bool   [required]
    flake      string [required]
    access_ip  string [required]
    priority   u8     [required]
}

pub fn sh(cmd string) os.Result {
    println('λ ${cmd}')
    return execute(cmd)
}

pub fn get_machines() ![]Machine {
    machines_query := sh('terraform -chdir=./terraform/ output -json | jq ".machines.value"')

    if machines_query.exit_code != 0 {
        return error("can't fetch terraform output")
    }

    return json.decode([]Machine, machines_query.output)!
}