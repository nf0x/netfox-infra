#!/usr/bin/env -S v

import os
import cli
import scripts { Machine, get_machines, sh, build_shared_state }

fn deploy(machines []Machine, bastion ?Machine)! {
	mut sorted_machines := machines.clone()
	sorted_machines.sort(a.priority < b.priority)

	for machine in sorted_machines {
		deploy_machine(machine, if machine.accessible { none } else { ?Machine(bastion) })!
	}
}

fn ssh_tunnel_open(machine Machine, bastion Machine) ! {
	res := sh('ssh -f -M -T -L 2222:${machine.access_ip}:22 -S ./netfox-handling-socket -N -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${bastion.access_ip}')

	if res.exit_code != 0 {
		return error(res.output)
	}

	// needed so nixos-rebuild connects against configured port
	setenv('NIX_SSHOPTS', '-p 2222', true)
}

fn ssh_tunnel_close(bastion Machine) ! {
	res := sh('ssh -S ./netfox-handling-socket -O exit root@${bastion.access_ip}')

	if res.exit_code != 0 {
		return error(res.output)
	}

	// reset it so consequent nixos-rebuild calls don't try to use the tunnel
	setenv('NIX_SSHOPTS', '', true)
}

fn ssh_keyscan(machine Machine, bastion ?Machine) !string {
	command := 'ssh-keyscan ${machine.access_ip} 2> /dev/null | ssh-to-age 2> /dev/null'

	if bastion_ := bastion {
		res := sh('ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${bastion_.access_ip} ${command}')
		return res.output.trim_space()
	}

	res := sh(command)
	if res.exit_code != 0 {
		return error(res.output)
	}

	return res.output.trim_space()
}

fn deploy_machine(machine Machine, bastion ?Machine) ! {
	if !machine.accessible {
		bastion_ := bastion or { panic('bastion arg should be passed') }
		ssh_tunnel_open(machine, bastion_)!
	}

	update_sops_config(machine, bastion)!
	build_shared_state(machine.flake) or {
		if err.code() == 1 {
			println("info: machine ${machine.flake} has no secrets")
		} else {
			return err
		}
	}

	nixos_rebuild(machine)!

	if !machine.accessible {
		bastion_ := bastion or { panic('bastion arg should be passed') }
		ssh_tunnel_close(bastion_)!
	}
}

fn nixos_rebuild(target_host Machine) ! {
	setenv("NIX_SSHOPTS", "${getenv("NIX_SSHOPTS")} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null", true)
	println("using NIX_SSHOPTS: ${getenv("NIX_SSHOPTS")}")

	connection_ip := if target_host.accessible { target_host.access_ip } else { '127.0.0.1' }
	res := sh(nixos_rebuild_format_command(connection_ip, connection_ip, target_host.flake))

	if res.exit_code != 0 {
		return error(res.output)
	}
}

fn nixos_rebuild_format_command(build_host_ip string, target_host_ip string, flake string) string {
	return '
		nixos-rebuild switch
			--use-remote-sudo
            --build-host "root@${build_host_ip}"
            --target-host "root@${target_host_ip}"
            --fast --flake ".#${flake}"
            --show-trace
    '.replace('\n',
		' ').trim_space()
}

/*
 * It is dangerous in here, traveler
 * The land below is infected by cursed yaml substitutions!
 *
 * The correct way of doing this would be to use a YAML parser in order to
 * read and modify the .sops.yaml file. Sadly, as of vlang V 0.3.4 a7f84e7
 * there is no builtin YAML parser.
 *
 * Thus, we modify .sops.yaml by repeatedly calling yq (the go version) via
 * shell commands.
 *
 * Hey, it works!
*/

enum SopsTagState as u8 {
	doesnt_exist
	exists_eq
	exists_not_eq
}

struct ReturnSopsState {
	state             SopsTagState
	tagged_server_key ?string
	actual_server_key ?string
}

fn get_state_of_tag_in_sops_config(machine Machine, bastion ?Machine) !ReturnSopsState {
	server_tag := 'server_${machine.flake}'
	has_server_tag := sh('yq ".keys" .sops.yaml | grep "\\&${server_tag}\\s" || echo ""')

	if has_server_tag.exit_code != 0 {
		panic(has_server_tag.output)
	}

	if has_server_tag.output.trim_space() == '' {
		actual_server_key := ssh_keyscan(machine, bastion)!
		return ReturnSopsState{SopsTagState.doesnt_exist, none, actual_server_key}
	}

	tagged_server_key := sh('echo "${has_server_tag.output.trim_space()}" | yq ".[]"').output.trim_space()
	actual_server_key := ssh_keyscan(machine, bastion)!

	return ReturnSopsState{if tagged_server_key == actual_server_key {
		SopsTagState.exists_eq
	} else {
		SopsTagState.exists_not_eq
	}, tagged_server_key, actual_server_key}
}

fn update_sops_config(machine Machine, bastion ?Machine) ! {
	res := get_state_of_tag_in_sops_config(machine, bastion)!

	match res.state {
		.exists_eq {}
		.exists_not_eq {
			actual_server_key := res.actual_server_key or {
				panic('actual_server_key should be set')
			}

			tagged_server_key := res.tagged_server_key or {
				panic('tagged_server_key should be set')
			}

			sh('sed -i "s/${tagged_server_key}/${actual_server_key}/g" .sops.yaml')
		}
		.doesnt_exist {
			actual_server_key := res.actual_server_key or {
				panic('actual_server_key should be set')
			}

			sh('yq -i e \'${sops_format_new_key(machine, actual_server_key)}\' .sops.yaml')
			sh('yq -i e \'${sops_format_new_creation_rule(machine)}\' .sops.yaml')
		}
	}

	if exists('hosts/${machine.flake}/secrets/secrets.yml') {
		sh('sops updatekeys -y "./hosts/${machine.flake}/secrets/secrets.yml"')
	}
}

fn sops_format_new_key(machine Machine, machine_key string) string {
	server_tag := 'server_${machine.flake}'

	return '
	  .keys += ( "${machine_key}" | . anchor="${server_tag}" )
    '.replace('\n',
		' ').trim_space()
}

fn sops_format_new_creation_rule(machine Machine) string {
	server_tag := 'server_${machine.flake}'

	return '
	  .creation_rules += [{
	    "path_regex": "hosts\\/${machine.flake}\\/secrets\\/.*\\.(yml|json|bin)",
	    "key_groups": [(
	      { "age": null }
	      | .age[0] alias="admin_netfox"
	      | .age[1] alias="${server_tag}"
	    )]
	  }]
    '.replace('\n',
		' ').trim_space()
}

fn deploy_handler(cmd cli.Command)! {
	machines_ := get_machines()!
	machines  := if cmd.args.len == 0 {
		machines_
	} else {
		machines_.filter(cmd.args.contains(it.flake))
	}

	bastion := machines_.filter(fn (e Machine) bool {
		return e.flake == 'netnat'
	}).first()

	deploy(machines, bastion) or {
		eprintln(err)
		if exists('./netfox-handling-socket') {
			ssh_tunnel_close(bastion)!
		}
	}
}

fn main() {
	mut cmd := cli.Command {
		name: 'cli'
		description: 'wrapper around nixos-rebuild to handle deployment of nixOS configurations'
		version: '0.1.0'
		execute: deploy_handler
		required_args: 0
		usage: '<empty> or <machine1> <machine2> ...'
	}

	cmd.parse(os.args)
}