#!/usr/bin/env sh
# set -e

SCRIPT_PATH=$(dirname "$(realpath "$0")")
PASSWORD_STORE_DIR="${SCRIPT_PATH}/../.pass_vault/" pass "$@"