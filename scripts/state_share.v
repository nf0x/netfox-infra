module scripts

import scripts { get_machines }
import json
import os

struct MachineState {
    machine string
    handler fn()[]StateUnit
}

struct StateUnit {
    name   string
    secret bool
    @type  StateSourceType
    parser fn([]Machine) string
}

enum StateSourceType {
    machine
}

const (
    state = [
        MachineState {
            machine: "risunazu",
            handler: risunazu_specs
        }
    ]
)

pub fn build_shared_state(machine string)! {
    specs := get_shared_state_spec(machine) or {
        return error_with_code("machine has no shared state spec", 1)
    }.handler()
    machines := get_machines()!

    mut output_secret := map[string]string{}
    mut output_public := map[string]string{}

    for spec in specs.filter(!it.secret) {
        output_public[spec.name] = spec.parser(machines)
    }

    for spec in specs.filter(it.secret) {
        output_secret[spec.name] = spec.parser(machines)
    }

    if output_public.len > 0 {
        os.mkdir_all("./hosts/${machine}")!
        os.write_file("./hosts/${machine}/vars.json", json.encode_pretty(output_public))!
        sh('git add ./hosts/${machine}/vars.json')
    }

    if output_secret.len > 0 {
        os.mkdir_all("./hosts/${machine}/secrets")!
        os.write_file("./hosts/${machine}/secrets/dyn.json", json.encode_pretty(output_secret))!
        sh('sops --encrypt --in-place ./hosts/${machine}/secrets/dyn.json')
        sh('git add ./hosts/${machine}/secrets/dyn.json')
    }
}

fn get_shared_state_spec(machine string) ?MachineState {
    return state.filter(it.machine == machine)[0] or { return none }
}

fn risunazu_specs() []StateUnit {
    return [
        StateUnit { 'gitea_ip', false, StateSourceType.machine, fn(x []Machine) string { return x.filter(it.flake == 'gitea').first().access_ip } }
    ]
}