resource "aws_instance" "ec2_balancer" {
  ami           = local.nixos_22_11
  subnet_id     = module.networking.sn_public.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.kp_netfox.key_name

  vpc_security_group_ids = [
    aws_security_group.sg_allow_inc_https.id,
    aws_security_group.sg_allow_inc_ssh.id,
    aws_security_group.sg_allow_out_all.id,
    aws_security_group.sg_allow_ext_inc_tcp_53.id,
  ]

  root_block_device {
    volume_size = 30
  }

  tags = {
    Name  = "ec2_netfox_balancer"
    Flake = "risunazu"
  }
}

resource "aws_eip" "eip_balancer" {
  vpc      = true
  instance = aws_instance.ec2_balancer.id
}

resource "aws_security_group" "sg_allow_inc_https" {
  name        = "sg_allow_inc_https"
  description = "allow incoming HTTP and HTTPS requests"
  vpc_id      = module.networking.vpc.id

  ingress = [
    {
      description = "allow HTTP requests"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = [ "0.0.0.0/0" ]
      self        = true

      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
    },
    {
      description = "allow HTTPS requests"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = [ "0.0.0.0/0" ]
      self        = true

      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
    },
  ]
}
