#!/usr/bin/env sh

SCRIPT_PATH=$(dirname "$(realpath "$0")")
PASS=$SCRIPT_PATH/../scripts/pass_wrapper.sh

aws_access_key=$($PASS aws_access_key)
export TF_VAR_aws_access_key=$aws_access_key

aws_secret_key=$($PASS aws_secret_key)
export TF_VAR_aws_secret_key=$aws_secret_key