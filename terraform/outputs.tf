output "machines" {
  value = [
    {
      flake      = aws_instance.ec2_gitea.tags.Flake
      access_ip  = aws_instance.ec2_gitea.private_ip
      accessible = false
      priority   = 98
    },
    {
      flake      = aws_instance.ec2_balancer.tags.Flake
      access_ip  = aws_eip.eip_balancer.public_ip
      accessible = true
      priority   = 99
    },
    module.networking.ec2_nat
  ]
}

output "backup" {
  value = [
    {
      id   = aws_s3_bucket.s3_backup.id
      name = aws_s3_bucket.s3_backup.bucket
    }
  ]
}