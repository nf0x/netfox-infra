resource "aws_subnet" "sn_private" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 3, 2)
  availability_zone = "eu-west-3a"

  tags = {
    Name = "sn_netfox_private"
  }
}

resource "aws_route_table" "rt_private" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    network_interface_id = aws_network_interface.ni_nat.id
  }

  tags = {
    Name = "rt_netfox_private"
  }
}

resource "aws_route_table_association" "rta_private" {
  subnet_id      = aws_subnet.sn_private.id
  route_table_id = aws_route_table.rt_private.id
}