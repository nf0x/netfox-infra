resource "aws_instance" "ec2_nat" {
  instance_type = "t2.micro"
  ami           = var.nixos_22_11
  key_name      = var.kp_deployer_key_name

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.ni_nat.id
  }

  root_block_device {
    volume_size = 30
  }

  tags = {
    Name  = "ec2_netfox_nat"
    Flake = "netnat"
  }
}

resource "aws_eip" "eip_nat" {
  vpc      = true
  instance = aws_instance.ec2_nat.id
}

resource "aws_network_interface" "ni_nat" {
  subnet_id         = aws_subnet.sn_public.id
  source_dest_check = false
  security_groups   = [
    var.sg_allow_inc_ssh,
    aws_security_group.sg_nat.id,
  ]
}

resource "aws_security_group" "sg_nat" {
  name        = "sg_netfox_nat"
  description = "security group for the NAT instance"
  vpc_id      = aws_vpc.vpc.id

  ingress = [
    {
      description = "allow all in traffic from the VPC CIDR"
      cidr_blocks = [ aws_vpc.vpc.cidr_block ]
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      self        = true

      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
    },
  ]

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
}