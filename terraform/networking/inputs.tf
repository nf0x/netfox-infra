variable "nixos_22_11" {
  type = string
}

variable "kp_deployer_key_name" {
  type = string
}

variable "sg_allow_inc_ssh" {
  type = string
}