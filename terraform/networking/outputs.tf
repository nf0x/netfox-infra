output "vpc" {
  value = {
    id         = aws_vpc.vpc.id
    cidr_block = aws_vpc.vpc.cidr_block
  }
}

output "sn_public" {
  value = {
    id = aws_subnet.sn_public.id
  }
}

output "sn_private" {
  value = {
    id = aws_subnet.sn_private.id
  }
}

output "ec2_nat" {
  value = {
    flake      = aws_instance.ec2_nat.tags.Flake
    access_ip  = aws_eip.eip_nat.public_ip
    accessible = true
    priority   = 1
  }
}