resource "aws_subnet" "sn_public" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 3, 1)
  availability_zone = "eu-west-3a"

  tags = {
    Name = "sn_netfox_public"
  }
}

resource "aws_route_table" "rt_public" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "rt_netfox_public"
  }
}

resource "aws_route_table_association" "rta_public" {
  route_table_id = aws_route_table.rt_public.id
  subnet_id      = aws_subnet.sn_public.id
}