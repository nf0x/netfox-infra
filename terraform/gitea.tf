resource "aws_instance" "ec2_gitea" {
  ami           = local.nixos_22_11
  subnet_id     = module.networking.sn_private.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.kp_netfox.key_name

  vpc_security_group_ids = [
    aws_security_group.sg_allow_inc_ssh.id,
    aws_security_group.sg_allow_out_all.id,
    aws_security_group.sg_allow_int_inc_tcp_3001.id,
  ]

  root_block_device {
    volume_size = 30
  }

  tags = {
    Name  = "ec2_netfox_gitea"
    Flake = "gitea"
  }
}

resource "aws_security_group" "sg_allow_int_inc_tcp_3001" {
  name        = "sg_allow_inc_3001_tcp"
  description = "allow internal incoming tcp requests on port 3001"
  vpc_id      = module.networking.vpc.id

  ingress {
    from_port = 3001
    to_port   = 3001
    protocol  = "tcp"
    cidr_blocks = [ module.networking.vpc.cidr_block ]
  }
}