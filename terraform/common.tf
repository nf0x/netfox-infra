# keypair used on ec2 instances
resource "aws_key_pair" "kp_netfox" {
  key_name   = "kp_netfox"
  public_key = local.netfox_public_key
}

# allows the aws resource to receive ssh petitions
resource "aws_security_group" "sg_allow_inc_ssh" {
  name   = "sg_allow_inc_ssh"
  vpc_id = module.networking.vpc.id

  ingress {
    cidr_blocks = [ "0.0.0.0/0" ]

    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }
}

# allows the aws resource to receive ssh petitions
resource "aws_security_group" "sg_allow_ext_inc_tcp_53" {
  name   = "sg_allow_ext_inc_tcp_53"
  vpc_id = module.networking.vpc.id

  ingress {
    cidr_blocks = [ "0.0.0.0/0" ]

    from_port = 53
    to_port   = 53
    protocol  = "tcp"
  }
}

# allows the aws resource to send any kind of traffic outside the vpc
resource "aws_security_group" "sg_allow_out_all" {
  name   = "sg_allow_out_all"
  vpc_id = module.networking.vpc.id

  egress {
    cidr_blocks = [ "0.0.0.0/0" ]

    from_port = 0
    to_port   = 0
    protocol  = "-1"
  }
}
