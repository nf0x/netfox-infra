variable "aws_access_key" {
  sensitive   = true
  type        = string
  description = "API access key to deploy to aws"
}

variable "aws_secret_key" {
  sensitive   = true
  type        = string
  description = "API secret key to deploy to aws"
}

provider "aws" {
  region     = "eu-west-3"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

locals {
  nixos_22_11        = module.nixos_22_11_eu_west_3.ami
  netfox_public_key  = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9s0FXfRZJ9C7eNz9lOZ3kL58kPAZZKsEmRZ0jlON5H4E/v8+hdFmdO4QiTOzKORQ1/JKJv9B71wO4+Eb0780quXHoukyqWCMF2xDiDIuyaT88CVl7HNHnE9aqTcSJcUYfj6jsJKOivSx7hrlyGJtDrPTAC1Y5UAi1EtgoktCHrhwR2E+mDhou2YtDe2UJ7Syk02/brKQd7qzIGVHHLTxngVY0neuoO6Yk2y6M+52QAK7569sSx8Q6n4SpY/JeE1x3hyCOEu9k/Tn4zlt7Cpluh9Dc6BxPNMKYi0CADbz9t3T/wc1grQ4AfJjifdvByXbFiMyFZD7eRySxjeP3IHwLlgsqn44i4YzwVyFA6qtveVf0xouWtYpWfAjcf0imwf7Q1fhsN1j4zUf6GsdFsdR+EquQHwOZJGlqZAKePBGcYkrM2EsC0+L8Cg9Dm1flxkYuuS1jc8rTeVhlYn5b3rOwQ5WWy7R4f17x/RwSxbghtUkvh6toTcAQk5BYBCIrz5BmfJCK/LOsQAR5Ts11T15P9En4zW93sRKdYfgKOK4KLRPQjoe2Eg6WTkgHyhc7nTTs5RMcpZ5tHSChVfJPuJWtsZ3NCJuqqhYTnU0UKZhHl3WHmTdato5ZpLe8apgVbb6sREZkYFppXu0Nwz54yvCCHxJzqxXh3QQSOd7wQTZNWQ== cardno:9_928_870"
}

module "networking" {
  source    = "./networking"
  providers = {
    aws = aws
  }

  kp_deployer_key_name    = aws_key_pair.kp_netfox.key_name
  nixos_22_11             = local.nixos_22_11
  sg_allow_inc_ssh        = aws_security_group.sg_allow_inc_ssh.id
}

module "nixos_22_11_eu_west_3" {
  source  = "github.com/Gabriella439/terraform-nixos-ng//ami?ref=d8563d06cc65bc699ffbf1ab8d692b1343ecd927"
  release = "22.11"
  region  = "eu-west-3"
}
