resource "aws_route53_zone" "r53_netfox" {
  name = "netfox.rip"
}

resource "aws_route53_record" "A" {
  name    = "git.netfox.rip"
  type    = "A"
  zone_id = aws_route53_zone.r53_netfox.id
  ttl     = 300
  records = [ aws_eip.eip_balancer.public_ip ]
}