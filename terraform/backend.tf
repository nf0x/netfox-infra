terraform {
  backend "s3" {
    bucket = "s3-netfox-tf-state"
    key    = "global/s3/terraform.tfstate"
    region = "eu-west-3"

    dynamodb_table = "dynt_netfox_tf_state_lock"
    encrypt        = true
  }
}

resource "aws_s3_bucket" "s3_netfox_tf_state" {
  bucket = "s3-netfox-tf-state"

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_versioning" "s3v_netfox_tf_state" {
  bucket = aws_s3_bucket.s3_netfox_tf_state.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "s3ssec_netfox_tf_state" {
  bucket = aws_s3_bucket.s3_netfox_tf_state.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "s3_bpab_netfox_tf_state" {
  bucket                  = aws_s3_bucket.s3_netfox_tf_state.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_dynamodb_table" "dynt_netfox_tf_state_lock" {
  name         = "dynt_netfox_tf_state_lock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}