{ modulesPath, pkgs, lib, config, ... }:
let
  vars = builtins.fromJSON (builtins.readFile ./vars.json);
in {
  imports = [
    "${modulesPath}/virtualisation/amazon-image.nix"
  ];
  ec2.hvm = true;
  system.stateVersion = "22.11";

  environment.systemPackages = with pkgs; [
    nginx
  ];

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9s0FXfRZJ9C7eNz9lOZ3kL58kPAZZKsEmRZ0jlON5H4E/v8+hdFmdO4QiTOzKORQ1/JKJv9B71wO4+Eb0780quXHoukyqWCMF2xDiDIuyaT88CVl7HNHnE9aqTcSJcUYfj6jsJKOivSx7hrlyGJtDrPTAC1Y5UAi1EtgoktCHrhwR2E+mDhou2YtDe2UJ7Syk02/brKQd7qzIGVHHLTxngVY0neuoO6Yk2y6M+52QAK7569sSx8Q6n4SpY/JeE1x3hyCOEu9k/Tn4zlt7Cpluh9Dc6BxPNMKYi0CADbz9t3T/wc1grQ4AfJjifdvByXbFiMyFZD7eRySxjeP3IHwLlgsqn44i4YzwVyFA6qtveVf0xouWtYpWfAjcf0imwf7Q1fhsN1j4zUf6GsdFsdR+EquQHwOZJGlqZAKePBGcYkrM2EsC0+L8Cg9Dm1flxkYuuS1jc8rTeVhlYn5b3rOwQ5WWy7R4f17x/RwSxbghtUkvh6toTcAQk5BYBCIrz5BmfJCK/LOsQAR5Ts11T15P9En4zW93sRKdYfgKOK4KLRPQjoe2Eg6WTkgHyhc7nTTs5RMcpZ5tHSChVfJPuJWtsZ3NCJuqqhYTnU0UKZhHl3WHmTdato5ZpLe8apgVbb6sREZkYFppXu0Nwz54yvCCHxJzqxXh3QQSOd7wQTZNWQ== cardno:9_928_870"
  ];

  boot.growPartition = true;
  networking.firewall.allowedTCPPorts = [ 80 443 53 ];

  services.nginx = {
    enable = true;

    recommendedGzipSettings  = true;
    recommendedOptimisation  = true;
    recommendedProxySettings = true;
    recommendedTlsSettings   = true;

    virtualHosts."git.netfox.rip" = {
      forceSSL   = true;
      enableACME = true;
      locations."/".proxyPass = "http://${vars.gitea_ip}:3001/";
    };

    streamConfig = ''
      server {
        listen 53;
        proxy_pass ${vars.gitea_ip}:22;
      }
    '';
  };

  security.acme = {
    acceptTerms = true;
    email       = "say-hi@netfox.rip";
  };
}