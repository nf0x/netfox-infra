{ pkgs, config, ... }:
let
  systemd           = config.systemd;
  duplicacy_scripts = with pkgs; callPackage ../derivations/duplicacy_scripts.nix { };
in {
  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    duplicacy
  ];

  sops.secrets.aws_access_key = {
    owner = systemd.services.duplicacyInit.serviceConfig.User;
  };

  sops.secrets.aws_secret_key = {
    owner = systemd.services.duplicacyInit.serviceConfig.User;
  };

  systemd.services.duplicacyInit = {
    wantedBy    = [ "multi-user.target" ];
    after       = [ "network.target" ];
    description = "initializer for the duplicacy backup tool";

    serviceConfig = {
      Type             = "oneshot";
      User             = config.users.users.gitea.name;
      ExecStart        = "${duplicacy_scripts}/bin/duplicacy_init.sh";
      Environment      = [
        "BACKUP_DIR=${config.services.gitea.dump.backupDir}"
        "AWS_ACCESS_KEY_FILE=${config.sops.secrets.aws_access_key.path}"
        "AWS_SECRET_KEY_FILE=${config.sops.secrets.aws_secret_key.path}"
      ];
    };
  };

  systemd.timers.duplicacyBackup = {
    wantedBy = [ "timers.target" ];

    timerConfig = {
      OnCalendar = "* *-*-* *:30:00";
      Unit       = "duplicacyBackup.service";
    };
  };

  systemd.services.duplicacyBackup = {
    description   = "executes duplicacy backup on BACKUP_DIR";

    serviceConfig = {
      Type             = "oneshot";
      User             = config.users.users.gitea.name;
      ExecStart        = "${duplicacy_scripts}/bin/duplicacy_backup.sh";
      WorkingDirectory = config.services.gitea.dump.backupDir;
      Environment      = [
        "BACKUP_DIR=${config.services.gitea.dump.backupDir}"
        "AWS_ACCESS_KEY_FILE=${config.sops.secrets.aws_access_key.path}"
        "AWS_SECRET_KEY_FILE=${config.sops.secrets.aws_secret_key.path}"
      ];
    };
  };
}