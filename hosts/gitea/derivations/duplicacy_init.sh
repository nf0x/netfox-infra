set -ex

SCRIPT_PATH=$(dirname "$(realpath "$0")")

if [ -z "$BACKUP_DIR" ]; then
  >&2 echo "error: BACKUP_DIR env var not defined"
  return 1
fi

if [ -z "$AWS_ACCESS_KEY_FILE" ]; then
  >&2 echo "error: AWS_ACCESS_KEY_FILE env var not defined"
  return 1
fi

if [ -z "$AWS_SECRET_KEY_FILE" ]; then
  >&2 echo "error: AWS_SECRET_KEY_FILE env var not defined"
  return 1
fi

expect  "$SCRIPT_PATH"/duplicacy_init.exp \
        "${BACKUP_DIR}"                   \
        "${HOME}"                         \
        "$(cat "${AWS_ACCESS_KEY_FILE}")" \
        "$(cat "${AWS_SECRET_KEY_FILE}")"