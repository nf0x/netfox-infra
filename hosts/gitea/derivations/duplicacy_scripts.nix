{ stdenv, lib, expect, duplicacy, coreutils, makeWrapper }:

stdenv.mkDerivation rec {
  name    = "duplicacy-scripts";
  phases  = "installPhase";
  src     = ./.;
  nativeBuildInputs = [ expect duplicacy coreutils makeWrapper ];

  installPhase = ''
    mkdir -p $out/bin

    cp $src/duplicacy_init.sh  $out/bin/duplicacy_init.sh
    cp $src/duplicacy_init.exp $out/bin/duplicacy_init.exp
    chmod +x $out/bin/duplicacy_init.sh

    cp $src/duplicacy_backup.sh  $out/bin/duplicacy_backup.sh
    cp $src/duplicacy_backup.exp $out/bin/duplicacy_backup.exp
    chmod +x $out/bin/duplicacy_backup.sh

    wrapProgram $out/bin/duplicacy_init.sh \
            --set PATH ${lib.makeBinPath [ expect duplicacy coreutils ]}

    wrapProgram $out/bin/duplicacy_backup.sh \
            --set PATH ${lib.makeBinPath [ expect duplicacy coreutils ]}
  '';
}