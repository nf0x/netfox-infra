{
  description = "build process for NixOS system configurations";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    sops-nix.url = "github:Mic92/sops-nix";
  };

  outputs = { self, nixpkgs, sops-nix, ...}@inputs:
    let
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      forAllSystems    = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor       = forAllSystems (system: import nixpkgs { inherit system; });
    in {
      nixosConfigurations = {
        gitea = nixpkgs.lib.nixosSystem {
          system  = "x86_64-linux";

          modules = [
            ./hosts/gitea/configuration.nix
            sops-nix.nixosModules.sops
          ];
        };

        netnat = nixpkgs.lib.nixosSystem {
          system  = "x86_64-linux";

          modules = [
            ./hosts/netnat/configuration.nix
            sops-nix.nixosModules.sops
          ];
        };

        risunazu = nixpkgs.lib.nixosSystem {
          system  = "x86_64-linux";

          modules = [
            ./hosts/risunazu/configuration.nix
            sops-nix.nixosModules.sops
          ];
        };
      };

      devShells = forAllSystems (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
          default = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [
              terraform
              packer
              nixos-rebuild
              coreutils
              jq
              yq-go
              sops
              ssh-to-age
            ];
          };
        });
    };
}
